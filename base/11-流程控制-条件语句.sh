echo "\n================= ↓↓↓↓↓↓ if-else ↓↓↓↓↓↓ ================="

a=10
b=20

if [ $a == $b ]
then
   echo "a 等于 b"
elif [ $a -gt $b ]
then
   echo "a 大于 b"
elif [ $a -lt $b ]
then
   echo "a 小于 b"
else
   echo "没有符合的条件"
fi

# 或
if [ $a == $b ]; then echo "a = b"; else echo "a != b"; fi


echo "\n================= ↓↓↓↓↓↓ case ... esac ↓↓↓↓↓↓ ================="

echo '请输入1到4之间的数字:'
echo '你输入的数字为:'
read num
case ${num} in
    1)  echo '你选择了 1'
    ;;
    2)  echo '你选择了 2'
    ;;
    3)  echo '你选择了 3'
    ;;
    4)  echo '你选择了 4'
    ;;
    *)  echo '你没有输入1到4之间的数字！'
    ;;
esac
